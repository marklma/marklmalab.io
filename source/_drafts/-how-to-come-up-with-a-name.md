---
title: 【Python3学习】我是怎么用Python给娃起名字的
tags:
  - Python3学习
  - pandas
categories:
  - 编程
date: 2023-02-19 08:50:04
description:
---

要怎么给自己的孩子起名字呢？身为一个会使用Python编写简单程序的人，我是这么做的。

## 思路

刘慈欣在2003年发表过一个中篇小说《诗云》（又名《李白》），主要说的是一个超高等外星文明为了写出超越李白的诗歌，穷尽了太阳系的大部分能量，列举出了所有可能的字词组合，创造出了”诗云“，但终因为无法从诗云中识别出优秀的诗歌，而宣告失败。

从大刘的这个故事就能发现起名字的方法，对了，那就是<del>穷尽地球或太阳系的能量……</del>穷举并选择。为了减少选择项目，我们首先就是增加一些**限制条件**，然后通过一些规则不断筛选，最终就能选到心仪的姓名。

<!-- more -->

## 步骤

### 增加限制条件

给自己增加限制条件一般就是求助怪力乱神了，虽然不信，但是也能让老人们开心，何乐而不为。老人通过生辰八字、人运天格等，总而言之，言而总之最终得出了姓名笔画以及搭配的限制条件：姓名要是3个字，名字前后两个字在康熙字典中笔画分别有4种组合方式：`[(21,20),(20,4),(12,12),(10,14)]`。

### 数据获取

有了限制条件，接下来就是考虑如何获取数据，首先是获取笔画为21、20、4、12、10、14的字是哪些。通过百度“康熙字典 笔画21”之类的搜索条件，就可以查出相应文字。
将所有文字复制到`all{笔画}.csv`的文件中，使用[正则表达式](/2013/the-power-of-regular-expression-1/)结合[notepad++](/2023/what-pc-tools-do-i-use-for-work/#notepad)，很快调整为一个字一行。

如文件`all4.csv`：
```
卬
方
户
化
今
午
心
尹
予
月
匀
中
```

### 再做筛选

有了数据，理论上只要排列组合即可出现备选名字，但是由于文字数过多，排列组合上万了，肯定都看不过来了，于是我需要再次精简内容，这回我就是直接删除文字，比如有的看着不吉利的文字就不要了，比如“乏”“仇”这种第一眼就先删掉了。

初步筛选后，我发现有很多字我不<del>认识</del>熟悉，或者不了解其含义是否能拿来做名字，于是我打算获取每个文字的意思，然后再做一次筛选。

这时候就需要爬虫上场了。