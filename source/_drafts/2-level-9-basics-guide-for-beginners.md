---
title: 【暗黑2 LLD介绍】9级PvP（VLLD）新手指导
tags:
  - 暗黑2
  - 低等级PvP
  - VLLD
categories:
  - 游戏
date: 2023-01-30 23:34:02
description:
---

[Level 9 Basics Guide For Beginners - Topic - d2jsp](https://forums.d2jsp.org/topic.php?t=5905428&f=143)
<!-- more -->
I've noticed more people are becoming interested in VLLD, so I got bored and decided to write this guide ![wink.gif](http://forums.d2jsp.org/html/emoticons/wink.gif)  
  
**Items**  
Since level 9 chars are very dependant on items, I think this needs to come first because it's the most important. First I'll start with some info on some cheap budget items, because VLLD can get extremely expensive and not all of us can afford it (I'm poor myself and I only just manage to do it). So here are some budget alternatives to the most expensive pieces of equipment on a VLLD character, helms, armor, weapons, shields:  
  
Biggins Bonnet Cap: Now this is a very nice item. It has Damage, Attack Rating, Life, and Mana. What more could you need? It can only get one socket but the chances are if you're poor enough to be using this then you'll only be able to afford a few jewels anyway. It is a nice helm especially for Jab Zons who can't find some good Enhanced Damage jewels for their shield or armor. But make sure you don't use any Maximum Damage jewels in this because of the ED/Max bug.  
  
Cleglaws sword: This is probably only useful on a Sac pala or maybe a Bash barb, but it gives 50% Deadly Strike and a nice boost to attack rating, and has -10 base speed. It's quite a nice weapon and even some level 18's use it.  
  
Pelta Lunata Shield: Now in my opinion this is an amazing and almost godly shield, the only drawback is that it can only get 1 socket. Apart from that, it has the best blocking available at level 9, and has Vitality and Energy, aswell as a small amount of Strength and good defence. I found this shield useful on my old Tiger Strike assassin. This shield will let Amazons, Assassins and Paladins block faster than anything else at level 9. Although this shield is probably only useful on an assassin.  
  
Bloodfist Gloves: These are also cheap but alot of people use them anyway, even on expensive builds. They have very nice mods, 40 Life, 5 Minimum Damage, 30FHR, 10IAS...they're amazing gloves, their only letdown is that they only have 10IAS.  
  
Deaths Belt: Again another cheap item that is used on more expensive builds aswell as cheap ones. This is used for "Cannot be Frozen", and when used with Deaths Gloves you get 30 IAS and 15 All res. It's nice to use against people with cold damage and many people think there's no alternative to this, although there is.  
  
Deaths Gloves: Only useful if using Deaths belt, these are the same as Bloodfists and Deaths Belt, sometimes used on expensive builds but don't cost anything. With Deaths Belt and Gloves you get 30IAS, 15 All Res, and CBF from the belt.  
  
Nagelring: If you can get these with 65 or more attack rating (70 or more is better) then they are nice cheap rings if you can't find or afford some rare ones.  
  
Sigons set: The Boots, Belt, Gloves and Helm are useful. 3 pieces together give 100 defence, the gloves and another piece give 30IAS, the boots give 50 attack rating, the belt gives defence per level and the helm gives attack rating per level. Many people dispize this set because when they hear the word "Sigon" it makes them think of noobs. But I think it has some fucking amazing mods, and I use it myself on my level 9.  
  
(Vulpine) Bone Shield of Deflecting: It doesn't have to have the Vulpine (Damage goes to mana) prefix but it's useful to have if you use skills that cost mana. You can buy these from Drognan in norm and then get 2 sockets in them at Larzuk. They are used on Bash barbs, Sac Necros, TS Assassins, Kicksins, and I use one on my Sac Barb. When people say "30/20" they are usually refering to one of these shields, since it has the "Deflecting" mod which gives 30/20 chance to block and blocking speed to your blocking.  
  
Jewels  
Now these need explaining in a bit more detail since level 9 chars depend heavily on jewels.  
The highest possible stats are:  
14 Maximum Damage  
6 Minimum Damage (not sure on this)  
20 Enhanced Damage  
40 Attack Rating  
12% Damage goes to Mana  
-15% Requirements  
  
Some useful ones for weapons would be:  
9 Max Damage/20 ED  
9 Max Damage/40 AR  
6 Min Damage/20ED  
etc...  
and you can keep going with the combinations aswell as plain jewels.  
  
Some useful ones for armor would be:  
9 Max Damage/-15% Requirements  
9 Max Damage/40AR/-15% Requirements  
And again, there are many many combinations possible.  
And you don't have to be limited to perfect stats. Something like 8 Max Damage/15 Enhanced Damage is still useful in a weapon.  
  
Oh and if you don't know about the ED/Max Dmg bug, well here it is: If you have ED/Max Dmg on armor/helm/shields, the ED only applies to the minimum damage, and vice versa (if you have Min Dmg and ED the ED only applies to the Max Dmg. This bug doesn't apply to weapons, so you can have ED and Max Damage on weapons and still get all the benefits. That's pretty much how it is, just ask other people if you want it explained in more detail. Just don't go wasting some godly jewels.  
  
The average level 9 Jewel is 9 Maximum Damage. Wherever I say 9 Max Damage, remember you could use better jewels than that you don't have to stick to 9 Max Dmg. I only say 9 Max because they're common, cheap, and easy to get. Although something like a Jab Amazon would have one item filled with Enhanced Damage jewels, because they don't get much Enhanced Damage from their skills  
  
  
Now that you've seen the nice cheap items and you know about the jewels I'll move onto something a little more expensive... although it's not expensive at all really. Things like plain 9 Max Damage jewels cost around 1-5fg each anyway.  
  
Helms: 3 Socket Mask or Crown depending on build, socketed with 3x 9 Max Damage jewels.  
  
Armor: 3 Socket Light Plate or Breast Plate socketed with 3x 9 Max Damage jewels.  
  
Shield: For Paladins, a 4 Socket Rondache with ED/AR (65%ED/121AR is perfect) socketed with 4x 9 Max Damage jewels, for anything else, an Amazon should use a 3 Socket Large shield with 3x 9 Max Damage jewels or 3x ED jewels depending on where they want to put their ED jewels (shield, armor or helm), and anything else should use a 30/20 Bone Shield with 2x 9 Max Damage jewels.  
  
**Stats & Skills**  
This is how you should have your stat points 99% of the time:  
Strength: Enough to wear items.  
Dexterity: Enough for Max Block. (This is important).  
Vitality: All spare points.  
Energy: None.  
Skills are different for each class, but they should be easy to work out anyway. It's too much effort to write out each skill setup for each build so I wont bother.  
  
**Level 9 Character Builds**  
Some level 9 chars are more common than others - there are a few "cookie cutter" builds, and some are one of a kind. As far as I know I have the only Sac Barb on my realm (EUSCL).  
  
Most common builds (cookie cutters):  
Tiger Strike Assassins - fast but need to charge up before they can do big amounts of damage, cheap to build  
Jab Amazons - fast and have dodge but generally have lower damage and attack rating than others, cheap to build  
Sacrifice Paladins (Offensive and Defensive) - have high damage but require alot of jewels, can be expensive  
Bash Barbs - low damage compared to other builds such as sac palas but have knockback and higher defence, can be expensive - Linny says they have high damage but compared to Sac Pala they don't IMO  
These are the standard of level 9 dueling, the most commonly seen builds.  
  
Less common builds:  
Kicksins - quite weak but fun and cheap  
Smiters - very very cheap, and can do quite well if built properly  
Bowazons - they are usually bow/jab hybrids,  
These builds are usually made for just fun builds but some Smiters can be pretty good.  
  
Rare builds:  
Sacrifice Necro - quite expensive, can have high damage similiar to Sac Pala but have lower attack rating, but they do have Golem + Bone Armor, they are rare  
Sacrifice Barb - can be expensive, same as Sac Necro, but lower damage, big attack rating and defence, but once the Sac Charges run out then they arent as good  
With any character using Sacrifice charges, it's best to get the duel finished quicker before the charges run out.  
  
Casters:  
Ice Sorc - not sure, never seen one  
Boulder Druid - can do quite good in duels if used properly, annoying to duel vs them because they don't fucking keep still  
Casters aren't very common but can do quite well if played properly.  
  
  
Well I can't think of much else, if I do think of anything I'll add it in.  
I hope this will help some people who want to start level 9 dueling.


Max mini on level 9 jewel is 8 (4 + 4)