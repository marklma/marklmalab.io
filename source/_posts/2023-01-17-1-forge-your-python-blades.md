---
title: 【Python3学习】1.铸刃磨刀：优化conda，配置IDE编写你的第一个Python程序
date: 2023-01-17 08:49:24
tags:
- Python3学习
- conda
- vscode
categories:
- 编程
description:
---

> 工欲善其事必先利其器。——《论语·卫灵公》

通过安装miniconda，Python和conda都完美安装了，基础环境有了，现在我们要优化一下环境，并选一些趁手的编程工具便于后续学习和使用。

## conda的设置

由于Python的版本比较多，并且它的库也非常广泛，同时库和库之间存在很多依赖关系，所以在库的安装和版本的管理上很麻烦。

而Conda是一个便捷管理Python版本和环境的工具，但直接通过`conda install`命令安装Python库，下载速度会让你窒息，频繁出现下载失败也会让你抓狂，为此我们需要进行一些配置才能快速安装常用Python库。
<!-- more -->
### （一）修改conda源

首先要解决conda默认源问题，这里又要搬出之前的[清华大学anaconda镜像站](https://mirrors.tuna.tsinghua.edu.cn/help/anaconda/)了，直接按照链接中指导操作即可，简要介绍如下：

1. 打开`Anaconda Prompt`，运行：
    ```
    conda config --set show_channel_urls yes
    ```
2. 随后在`C:\Users\用户名\`目录下找`.condrac`文件，右键用记事本打开，输入：
    ```
    channels:
    - defaults
    show_channel_urls: true
    default_channels:
    - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main
    - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/r
    - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/msys2
    custom_channels:
    conda-forge: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
    msys2: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
    bioconda: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
    menpo: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
    pytorch: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
    pytorch-lts: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
    simpleitk: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
    ```
    **保存**后即可添加 Anaconda Python 免费仓库。
3. 输入`conda clean -i`清除索引缓存，保证用的是镜像站提供的索引。

### （二）安装常用库

打开`Anaconda Prompt`，输入运行：
```
conda install notebook matplotlib pandas pillow requests xlrd xlsxwriter xlwings ipython openpyxl
```
基本安装了之后工作中会频繁用到的Python库了。

## IDE安装

Python编程使用记事本和简单命令行即可操作实现，不过为了便利，我们还是考虑使用一些工具，比如IDE[^ide]等。当然也不用追求什么最好，自己用得爽，你用notepad也OK。

一般推荐pycharm、vscode、sublime text，甚至notepad++等都可以。虽然pycharm很强很好用，但是架不住~~穷~~收费要求，笔者也不想要什么破解版，故如果电脑配置不是太差的，推荐使用vscode。

## vscode安装和配置

VScode全称是Visual Studio Code，是微软推出的一个跨平台的编辑器，能够在Windows、Linux、iOS等平台上运行，通过安装一些插件可以让这个编辑器变成一个编译器。由于笔者身患有绿软就要用的病，这里就主要介绍绿色版vscode如何安装使用。

### （一）下载安装vscode
1. 首先登陆vscode下载网站[https://code.visualstudio.com/Download](https://code.visualstudio.com/Download)，选择对应操作系统版本，比如笔者这里就使用了Windows x64环境下的`.zip`版本。
2. 下载完成后将内容解压至`D:\dev\vscode`目录下。
3. 可以将文件夹下的`Code.exe`生成快捷方式到桌面后，双击运行。

### （二）插件安装和配置
1. 点击左边栏四个方块那个“扩展”功能图标，等待一会儿会跳出很多插件，可以先安装如下几个插件：
    - Python
    - Jupyter
    - Chinese (Simplified) (简体中文) Language Pack for Visual Studio Code
    ![](1-forge-your-python-blades_20230119082734.png)
2. 安装完毕最后一个插件后，右下角会跳出需要切换语言的指示，点击切换并重启vscode，可以看到中文界面了。
3. 再次点击扩展图标（左边栏四个方块图标），找到“已安装”里面的`Python`插件，右键点击->扩展设置，在右侧找到`Conda Path`，输入框中输入`_conda.exe`的安装路径：`D:\dev\miniconda3\_conda.exe`。
4. 找到`Default Interpreter Path`，输入框中输入`D:\dev\miniconda3\python.exe`。
    ![](1-forge-your-python-blades_20230119084039.png)
5. 配置完后按`ctrl+s`保存即可。

### （三）再来个hello world

有两种方式来运行python程序，

- 首先是比较常见的运行`.py`文件的方法。
1. 新建一个.py文件，点击左上角文件->新建文本文件->跳出内容中输入：
    ```python
    # -*- coding: utf-8 -*-
    print('hello world!')
    ```
    `ctrl+s`将文件保存在任意目录，比如我保存为`D:\workspace\Untitled-1.py`。
2. 按`F5` 后，看下面运行结果接口。

- 还有就是类似运行ipython，直接采用交互式编程。
1. 新建一个.py文件，点击左上角文件->新建文本文件->跳出内容中输入：
    ```python
    # -*- coding: utf-8 -*-
    # %%
    print('hello world!')
    ```
    注意到区别了么？我在要运行的命令上写了个`# %%`，写上后等一下，就能在上面发现有`Run Cell`字样，点击后即可新开一个tab，在对应tab中运行对应脚本。此时你可以继续在新tab的最下面继续书。
2. 以上操作实际上和你打开`Anaconda Prompt`，输入运行`ipython`异曲同工，都来试试看吧。

**恭喜你，你现在可以在自己简历里写上：编写过Python程序了！**

[^ide]: 集成开发环境（IDE，Integrated Development Environment ）是用于提供程序开发环境的应用程序，一般包括代码编辑器、编译器、调试器和图形用户界面等工具。