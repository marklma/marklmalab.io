---
title: 【Python3学习-番外篇】Python操作硬件，这好玩吗？
tags:
  - MicroPython
  - ESP8266
  - 硬件
categories:
  - 编程
date: 2023-04-02 16:42:12
description:
---

除了提升日常工作效率，Python也有一些好玩的内容，毕竟成年人的自学除了动机最重要的就是兴趣了吧。我这不是刚到35，就突然买了一些以前从未接触过的单片机硬件结合[MicroPython](https://micropython.org/)来玩，看看是否能给大家抛砖引玉，提起Python学习兴趣。

<!-- more -->

## 初识

好几年前曾经买过树莓派4B，当时曾妄图自己弄个互联网电视监控硬件，刚摆弄了几下操作系统，就放在那里吃灰了。

近期突发奇想，看看能不能想弄一些硬件解决工作中的问题，而树莓派的高价已经挡住了去路。这时候我发现了单片机的价格已经大大出乎我的意料，一个MCU，也就是微控制单元(Microcontroller Unit；MCU) ，价格可能才10元，结合一些外设，用MicroPython编程就能做出机械小车等东西，这就引起了我的兴趣。

## 出手

在B站上逛了一下，发现硬件/单片机入门门槛确实不高，于是就斥~~巨~~资买了ESP8266、一些电阻、面包板、LED灯、杜邦线，还有杂七杂八等东西，价格高达70元。然后照着B站的介绍就做起了“一灯大师”——就是用单片机点亮一盏LED灯。

主要参考资料如下：
[轻玩科技肖华盛的个人空间_哔哩哔哩_bilibili](https://space.bilibili.com/393119260/channel/collectiondetail?sid=1153774)
[王大雷的学习进阶之路的个人空间_哔哩哔哩_bilibili](https://space.bilibili.com/85957698/channel/collectiondetail?sid=376054)

## 成果

按照B站大佬们的指导，我装了驱动、刷了rom，安装了[Thonny](https://thonny.org/)，编制了第一个MicroPython脚本，并成功通过ESP8266这个芯片点亮了LED灯，造轮子的喜悦油然而生，**中年男人的快乐就是这么单调朴素**。

### 闪烁的LED
```Python
from machine import Pin # 导入Pin
import time

led = Pin(4,Pin.OUT) # 意味着我的LED正极接入了ESP8266的GPIO4引脚，负极接入了GND
flag = False
while True:
    led.value(flag) # led的value是1就是亮灯，0就是灭灯
    time.sleep(0.5) # 暂停0.5秒
    flag = not flag
```
![成果1](yidengdashishanshuo.gif)

### 呼吸的LED
```Python
from machine import Pin,PWM
import time

led_pin = Pin(4,Pin.OUT)
led_pwm = PWM(led_pin,freq=1000,duty=0)

while True:
    for i in range(0,1023):
        led_pwm.duty(i)
        time.sleep_ms(1)
    time.sleep_ms(10)
    for i in range(1023,0,-1):
        led_pwm.duty(i)
        time.sleep_ms(1)
    time.sleep_ms(10)
```
![成果2](yidengdashihuxi.gif)

如果觉得直接学习[Python](https://markto.top/tags/python3%E5%AD%A6%E4%B9%A0/)没办法很快解决问题获得成就感，那么可以考虑像本文一样，从硬件开始学起，因为能更快看到成效，应该能更容易激发Python学习热情吧。