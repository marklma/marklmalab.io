---
title: 【Python3学习】4.vlookup/xlookup在python里的平替是什么？
tags:
  - Python3学习
  - pandas
categories:
  - 编程
date: 2023-01-29 22:27:23
description:
---

## 太长不看版本

对于超百万级数据，如何进行vlookup/xlookup？
答：只要使用pandas的[merge函数](http://c.biancheng.net/pandas/merge.html)即可。

### 案例数据准备
用文本编辑器输入以下内容，另存为`D:\workspace\in.csv`。
<!-- more -->
```
userid,loginaddr,starttime,state
123k,a1,20230128,SUC
124,a2,20230128,SUC
125,a3,20230128,SUC
126,a4,20230128,SUC
127,a5,20230128,SUC
128t,a6,20230128,SUC
129,a7,20230128,FAIL
0110,a8,20230128,SUC
123,a1,20230128,SUC
```
再建立一个文件，另存为`D:\workspace\addr.csv`。
```
userid,username,city
0110,user1,nn
123k,user2,bj
124,user3,sz
125,user4,sh
126,user5,zj
127,user6,js
128t,user7,fj
129,user8,yn
130,user9,gl
...pretend to,have 2000000,rows
```

### 具体操作
1. [利用pd.merge来匹配数据](#merge)

## 怎么匹配百万级数据？

审计工作正在如火如荼地进行，今天审计人员提供小马1000个用户编码，要求匹配用户名和注册地，虽说基础数据上百万用不了EXCEL，但是这种简单问题小马觉得只要数据库跑一跑就能解决了，可惜，小马不会数据库。

自己不会总有人会，他将头转向胖驼前辈：“驼哥，哦不，驼专，有空吗？”“你这是无事不登三宝殿，说吧，什么事？”胖驼瞥了一眼小马，并没有停下手中的活。

小马说：“这不有1000个号码，帮忙匹配一下用户账号和注册地呗，审计要。我这不会数据库地干活，只能来求您老出手了。”

胖驼还是头也不抬：“你是有基础数据文件在电脑里？”

“当然当然，提供给审计的原始文件我都存着，您可以随时导入数据库。”小马忙不迭地答道。

“那用啥数据库，基础数据才百万级，python跑一下就行了。”胖驼终于停下手中的活儿看向小马，“正好，你不是要转型么，那就和上次一样，我给你案例，你自己vlookup一下就好。”

“EXCEL vlookup我会呀，xlookup我都学了，不过基础数据超过104万了，excel匹配不完。”小马吐了吐舌头，“而且打得开电脑也死了吧。”

“哦，不不不，我这嘴瓢了，我是想说用python类似能力解决即可。”胖驼说。

### <a id='merge'>利用pd.merge来匹配数据</a>
```python
import pandas as pd # as always，先导入pandas包，取名叫做pd

# 将要导入的表赋值给in_file和addr_file
in_file = r"D:\workspace\in.csv"
addr_file = r"D:\workspace\addr.csv"

# 使用pd的read_csv能力读取csv文件，dtype意思是导入时候将内容看成什么数据类型，这里是指将userid这个字段看成字符串str，避免0开头的纯数字字段被当作整数了。
in_df = pd.read_csv(in_file,dtype={'userid':str},usecols=['userid','loginaddr','starttime']) # usecols是指这个表格只导入哪些列，这样可以减少内存损耗。这里还可以提一下，[]中括号中的内容就是列表list
addr_df = pd.read_csv(addr_file,dtype=str) # dtype=str意思是将所有字段都看成str。要提醒的是pandas匹配还是要考虑内存是否够大，如果文件超过4G，你电脑内存也就8G，考虑到后续还要匹配数据，基本也跑不动了。

in_df = pd.merge(in_df,addr_df,how='left',left_on='userid',right_on='userid') # pd.merge就是pandas中vlookup能力了，这里需要了解一下how的内容，这里我写'left'实际上就是数据库中的左连接（left join），可以理解为在in.csv表中，vlookup(in表的userid, addr表中userid至其他所有列, X, FALSE)，也就是根据userid匹配出所有结果，补齐在in表后。
in_df.to_excel('in.xlsx',index=False) # 输出成EXCEL，导出数据方法之一
```

胖驼：“你看，几句编程语言就能将文件读取、匹配、输出都搞定了，自己根据案例匹配一下数据看看。不懂就下面留言中补充吧！”