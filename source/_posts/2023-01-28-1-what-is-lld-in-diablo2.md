---
title: 【暗黑2 LLD介绍】超冷门玩法：低等级PvP（Low Level Dueling）简介
tags:
  - 暗黑2
  - 低等级PvP
  - LLD
categories:
  - 游戏
date: 2023-01-28 22:52:36
description:
---

暗黑2重制版新赛季即将开始，暗黑2相关资料、配装、速通、背景等各种信息也已“汗牛充栋”，但是我发现中文互联网中关于“低等级PvP”的内容还是空白。而低等级PvP这种玩法在欧美服还是有一些玩家（也是冷门），用过[d2jsp](https://forums.d2jsp.org)的伙伴应该会看见一些写着买卖LLD物品的帖子，里面的东西可能让初次见到的人觉得不可思议，比如1敏捷4准确率小符、9最大伤害珠宝等，而这些就是低等级PvP的基础物品。

为了水几篇博文，一个玩了十多年暗黑2，但是完全没玩过低等级PK的老玩家，决定翻译几篇介绍性内容，也不知道未来有没有人会开始传播和推广这种玩法。

<!-- more -->

## 常见问题-FAQ[^source]
  
### 什么是暗黑2中的低等级PK（LLD）?中等级PK呢（MLD）?
- LLD（Low Level Dueling），即低等级PK，玩家用9至30级人物进行PK的一种玩法，常见的LLD等级是9、12、15、18、29（欧服）和30级。
  （有时候9级PK会叫作vlld，即very low level dueling，超低等级PK，不过似乎是小众中的小众）
- MLD（Mid Level Dueling），即中等级PK，玩家用31至49级人物进行PK的一种玩法，常见的MLD等级是35、39、42和49级。

### 是否允许人物过地狱难度，并且做完所有的技能/属性任务？
- 允许的，并且强烈建议你这么做。

### 我能否到达XX级以便让我能使用XX装备？  
- 不行，必须是规定等级人物之间PK。

### 是否能与一个高等级角色组队，以便获得战吼、光环、强化等buff？
- 不行。
  
### 我该如何在游戏中发起PvP决斗？
- 在游戏中请求与对方决斗，并等待对方的回应。不要在没有回应的情况下直接发起攻击。
  
### 59级属于MLD范围么？
- 不属于，在59级算是HLD（高等级PvP），可以简单理解为能用高级符文的等级就算是高等级。

### 女巫是否允许用瞬移？
- 允许
  
### LLD中是否能使用蓝瓶？
- 只要你不是带满整个腰带的蓝瓶，哪里的蓝瓶都可以用。如果你真的需要那么多的法力，那你需要增加你的法力上限或法力恢复能力。（译者注：还是得看约定）

### LLD中使用什么技能会被认为是不礼貌的（BM，bad manner）？
- 慢速箭，骨牢（特别是对弓系亚马逊时候），祈祷（在等级9LLD时候），神圣冰冻光环，圣护或单抗光环，衰老诅咒，偷取生命诅咒

### LLD中装备哪些属性的物品会被认为是不礼貌的（BM，bad manner）？
- +抗性上限、减慢敌人速度%、击退（欧服）和任何BUG装备。
  
### LLD中哪些行为会被认为是不礼貌的（BM，bad manner）？
- 跑出屏幕外回血回蓝，nking，jumping等。 （额，没看懂那两个是啥，万望有大神指导）
  
### 我找到一个英气逼人的黄色圣骑士专属盾，是否适宜在LLD中使用？
- 不，在这个等级，对于施法者来说你有精神盾，对于近战肉搏者来说4孔高ed和ar的盾更好。
  
### 在等级29/30，小符的最大生命法力是多少？18级呢？
- 29、30lvl：15生命/12法力小符。
- 18lvl：15生命/7法力小符。

### 变身系德鲁伊是否需要高跑？如果需要，建议需要多少高跑？ 
- 是的，至少要达到110高跑（FRW）。
  
### 49级人物用的技能板能带的最大生命能有多少？
- 30 life。
  
### LLD中弓箭ama需要多少fhr？
- 不需要。
  
### 为什么在LLD里亚马逊更愿意使用harpoons鱼叉而非亚马逊专用的ceremonial javelins仪式标枪？
- 因为增加敏捷会增加使用鱼叉的伤害，但是不会增加亚马逊专用标枪的伤害。
  
## 有用的链接（以后再选取翻译）
- The Arreat Summit: [http://www.battle.net/diablo2exp/](http://www.battle.net/diablo2exp/)
- FHR/FCR/FBR Breakpoints for each class: [http://forums.d2jsp.org/index.php?showtopic=2450747](http://forums.d2jsp.org/index.php?showtopic=2450747)
- Kick damage calculator: [http://www.theamazonbasin.com/~mikeandroe/kickcalc/calc.php](http://www.theamazonbasin.com/~mikeandroe/kickcalc/calc.php)
- Character Armor Models: [http://forums.d2jsp.org/index.php?showtopic=9212540](http://forums.d2jsp.org/index.php?showtopic=9212540)  
- Level 30 - Items to Keep: [http://forums.d2jsp.org/index.php?showtopic=14161522](http://forums.d2jsp.org/index.php?showtopic=14161522)  
- Ste's Guide to Newcomer's: [http://forums.d2jsp.org/index.php?showtopic=5206618](http://forums.d2jsp.org/index.php?showtopic=5206618)

[^source]: 翻译来源：[Frequently Asked Question And Useful Links - Topic - d2jsp](https://forums.d2jsp.org/topic.php?t=29870086)