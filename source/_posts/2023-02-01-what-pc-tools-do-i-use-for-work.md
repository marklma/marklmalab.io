---
title: 提升效率软件推荐（电脑篇）
tags:
  - 软件
categories:
  - 工具
date: 2023-02-01 00:15:07
description:
---

> 工欲善其事必先利其器。——《论语•卫灵公》

除了学会编程可以提升效率，能积极学习使用一些优秀软件也可以极大提升工作效率。

## 编辑器
<!-- more -->
### notepad++
- **官网：** https://notepad-plus-plus.org/ ，可能需要科学上网，不过不要紧，我们可以从别的官方渠道下载：[Releases · notepad-plus-plus/notepad-plus-plus (github.com)](https://github.com/notepad-plus-plus/notepad-plus-plus/releases)，不排除有的童鞋还是无法访问，可以考虑网上找一些镜像后再下载。
- **优点：** 内存占用少、启动快，对于用惯notepad朋友，会发现他关闭后再打开内容不会丢失的功能很好用，可以有较多插件安装。
- **缺点：** 作者ZZ不正确，经常发表一些不HX的言论，所以网站才无法访问，可能引起有些人的不适，**那就别用好了**。还有就是它不像IDE有较多的编程提示，所以不太适合作为新手IDE使用。
- **替代方案：** [NotepadNext](https://github.com/dail8859/NotepadNext) 、sublimetext、[notepad--](https://gitee.com/cxasm/notepad--)等。

### Obsidian
- 官网：[Obsidian](https://obsidian.md/)
- 优点：如果会用Markdown语法写文章，我觉得Obsidian是一个很合适的写作软件。支持双链，纯文本编辑，有大量好用的插件，可以部分替代甚至平替notion、wolai之类的产品。本文就是通过Obsidian书写。
- 缺点：要美观、好用的话确实需要一些学习门槛。

### Effidit（智能创作助手）
- 官网：[Effidit (qq.com)](https://effidit.qq.com/)
- 优点：腾讯的良心软件之一，智能创作助手提供智能纠错、文本补全、文本改写、文本扩写、词语推荐、句子推荐与生成等功能，帮助您轻松完成中英文写作，提高写作效率。我主要用来纠错，当然注意上传云的东西要脱敏哦。
- 缺点：易用性方面待提升，专业领域还较少。

### 有道云笔记
- 官网：[有道云笔记｜亿万用户的选择 (youdao.com)](https://note.youdao.com/)
- 优点：免费大碗，功能齐全，模板、语音识别、OCR、分享应有尽有。笔者尝试了一些替代方案，感觉还是有道云比较好。
- 缺点：感觉APP同步等能力有待提升。

### XMind
- 官网：[Xmind思维导图 | Xmind中文官方网站](https://xmind.cn/)
- 优点：免费版就够用了，功能强大，可以备注、可以插入标注、可以统合，可以输出。
- 缺点：不少实用性功能还是要钱~我自己用的是Xmind8版本，感觉比现在最新版本要良心一点。
- 替代方案：有道云、怡氧就可以，在线的还有map.baidu.com等，虽然缺少F3的标注功能。

### 怡氧
- 官网：[怡氧|办公工具集|流程图|思维导图|大纲笔记|Office_怡氧官网 (jianguoyun.com)](https://cpclanding.jianguoyun.com/yiyang)
- 优点：[坚果云](https://www.jianguoyun.com/) 的轻插件前身，免费好用，比如思维导图、uml图、大纲笔记等应有尽有。见到相关功能我是震惊的，没想到坚果云还做了那么多良心插件。
- 替代方案：[ProcessOn - 免费在线作图，思维导图，流程图，实时协作](https://www.processon.com/i/544362e20cf2fa24a8f8d48b)、VSCode中Draw.io插件。

## 阅读器

### SumatraPDF
- 官网：[Free PDF Reader - Sumatra PDF (sumatrapdfreader.org)](https://www.sumatrapdfreader.org/free-pdf-reader)
- 优点：小巧轻快、绿色佳软。
- 缺点：修改pdf等高级功能就别想了。

### BookxNote Pro
- 官网：[BookxNote Pro电子学习笔记软件](http://www.bookxnote.com/)
- 优点：可以对PDF记笔记，甚至是全图的pdf都没问题。使用感觉也好。
- 缺点：可能支持的格式不太多吧。
- 替代方案：Obsidian也可以使用插件。

### 微信读书
- 官网：[微信读书-正版书籍小说免费阅读 (qq.com)](https://weread.qq.com/)
- 优点：大量免费书籍，免费功能够用，而且有模拟真人阅读的方式阅读书籍。即使遇到收费的书籍，能弄到epub格式也可以利用微信读书的智能阅读能力来听书，适合上下班途径听书（近期笔者正在听《人民的名义》来着~）。
- 缺点：不少书还是要会员。

## 辅助工具

### utools
- 官网：[uTools官网 - 新一代效率工具平台](https://u.tools/)
- 优点：通过快捷键（默认 `alt + space`，我用两下`ctrl` ）就可以快速呼出这个搜索框。你可以往输入框内粘贴文本、图片、截图、文件、文件夹等等，能够处理此内容的插件也早已准备就绪，统一的设计风格和操作方式，助你高效的得到结果。如不用鼠标调用everything软件查找电脑中文件、使用计算器、截图并悬浮、调用OCR，够了么？还有万千插件能你挖掘。
- 缺点：比调用[火柴(原火萤酱)](https://www.huochaipro.com/) 还多按一点。
- 替代方案：[火柴官网(原火萤酱)-文件秒搜|局域网聊天_电脑必备|效率神器 (huochaipro.com)](https://www.huochaipro.com/)

### Q-dir
- 官网：[Q-Dir the Quad Explorer](http://q-dir.com/)，我个人一般从[Q-Dir Download - The Quad Explorer (softwareok.com)](https://www.softwareok.com/?Download=Q-Dir)处下载
- 优点：绿色小巧，功能够用，可以平铺两个文件浏览器浏览等。
- 缺点：功能还不够强大，美观可能不足。

### 酷呆桌面
- 官网：[酷呆桌面 - 桌面整理工具 (coodesker.com)](https://www.coodesker.com/)
- 优点：可以重新涉及桌面排版，实现美观、多功能等。
- 替代方案：腾讯桌面。

### 万彩办公大师Officebox
- 官网：[万彩办公大师官网-免费的办公工具百宝箱OfficeBox,绿色无广告无捆绑 (wofficebox.com)](http://www.wofficebox.com/)
- 优点：各种常用工具集合。
- 缺点：感觉中文处理还是不足。

### 滴答清单
- 官网：[滴答清单:一个帮你高效完成任务和规划时间的应用 (dida365.com)](https://www.dida365.com/)
- 优点：感觉是目前最好的、全能的时间管理软件了吧。

### iSlide PPT插件
- 官网：[iSlide- 让PPT设计简单起来 | PPT模板下载平台](https://www.islide.cc/)
- 优点：支持wps，可以有很多免费图标、案例。
- 缺点：不过好像只支持个人版？

## 其他

### Hyper-V虚拟机
- 系统自带能力，随便找个教程即可安装，如[虚拟机的新选择，win10自带Hyper-V 虚拟机 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/141973142)
- 优点：windows原生，免费。
- 缺点：还是不如virtualbox感觉好用。
- 替代方案：[Oracle VM VirtualBox](https://www.virtualbox.org/)。

### Hexo
- 官网：[Hexo](https://hexo.io/zh-cn/)
- 优点：不用服务器制作静态页面博客，直接github、gitlab等即可免费部署，简单、模板多，国内用的人也多。我的博客就是用hexo生成。
- 缺点：使用的程序不是我了解的~
- 替代方案：从[Static Site Generators - Top Open Source SSGs | Jamstack](https://jamstack.org/generators/) 选吧。

### dbeaver
- 官网：[DBeaver Community | Free Universal Database Tool](https://dbeaver.io/)
- 优点：免费够用，弄个sqlite就能用来处理较大数据了。
- 缺点：中文处理还没能比得上navicat。

### 火绒安全
- 官网：[火绒安全 (huorong.cn)](https://www.huorong.cn/)
- 优点：强大、安全、静默，谁用谁知道。