---
title: 【Python3学习】6.如何合并文本（csv）文件？
tags:
  - Python3学习
categories:
  - 编程
date: 2023-04-29 23:29:32
description:
---

问：如何合并文本文件？

### 案例数据准备
`D:\workspace\in1.csv`
```
acc,grade
asdf,32
dfaga,15
adg,91
```
`D:\workspace\in2.csv`
```
acc,grade
am,25
kag,65
lkj,88
```

### 具体操作
<!-- more -->
`D:\workspace\combine_csv.py`
```Python
import glob # glob可以用来模糊搜索文件清单
with open('out.csv','wb') as f_out: # 用二进制写‘wb’的形式写入文件`out.csv`
	for fname in glob.glob('in*.csv'): # 搜索对应路径下in*.csv文件列表，逐一赋值给fname
		print(fname) # 显示搜索到的文件名
		with open(fname,'rb') as f_in: # 用二进制只读‘rb’的形式读取文件fname，复制给f_in
			for line in f_in: # 逐行读取文件，赋值给line
				f_out.write(line) # 将读取到的line输出至f_out
```

这样就能输出合并文件，不过我们发现合并的文件有两行标题`acc,grade`，我们希望的是第二个文件不用标题了，那就简单增加一个计数来解决。

```Python
import glob # glob可以用来模糊搜索文件清单
with open('out.csv','wb') as f_out: # 用二进制写‘wb’的形式写入文件`out.csv`
	i = 0
	for fname in glob.glob('in*.csv'): # 搜索对应路径下in*.csv文件列表，逐一赋值给fname
		print(fname) # 显示搜索到的文件名
		i+=1 # 增加计数，看看读取第几个文件
		with open(fname,'rb') as f_in: # 用二进制只读‘rb’的形式读取文件fname，复制给f_in
			if i>1:
				f_in.readline() # 如果读取到第二个文件起，那就跳过第一行，不用写入f_out
			for line in f_in: # 逐行读取文件，赋值给line
				f_out.write(line) # 将读取到的line输出至f_out
```
