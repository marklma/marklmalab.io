---
title: 【Python3学习】0.学习伊始：安装Python3
date: 2023-01-15 23:45:07
tags:
- Python3学习
- 入门
categories:
- 编程
description: 
---

你是否对超百万行的csv数据感到无所适从？  
你是否厌倦了每日制作相同报表，不断各种vlookup/xlookup？  
你是否对跑了半小时EXCEL数据也不知道是否能出结果的日常感到疲惫？  
你是否想进一步提升工作效率？  
那还等什么？ <del>还不换个工作？</del>  
快来学习学习python3！

## 自学途径

在现在这个年代，网上各种教程一抓一大把，这里我推荐有兴趣、有毅力的伙伴从以下途径学习：
<!-- more -->
首先是[廖雪峰的小白的Python新手教程](https://www.liaoxuefeng.com/wiki/1016959663602400)，特点正如他自称的：**中文，免费，零起点，完整示例，基于最新的Python 3版本**。

其次是[Python 教程 (w3school.com.cn)](https://www.w3school.com.cn/python/index.asp) 、 [Python3 教程 | 菜鸟教程 (runoob.com)](https://www.runoob.com/python3/python3-tutorial.html)
，这两个网站大同小异，基本上可以当作查询手册来学习了。

当然，如果想看一手资料，那就是[官网的入门教程](https://wiki.python.org/moin/BeginnersGuide)了，不过总的来说看中文教程还是推荐上面两个途径，不过如果不喜欢以上内容，也可以跟着往下看。

## 总之，先下载安装个Python3吧

一般推荐都是从[Python官网](https://www.python.org/)直接下载Python安装包来安装，笔者则推荐通过安装[Miniconda](https://docs.conda.io/en/latest/miniconda.html)来获取Python，除了能解决网页加载慢的问题之外，还能更容易进行Python的包管理。

> Miniconda 是一个 [Anaconda](https://www.anaconda.com/)的轻量级替代，默认只包含了 python 和 conda，但是可以通过 pip 和 conda 来安装所需要的包。

我们可以到[anaconda | 镜像站使用帮助 | 清华大学开源软件镜像站 | Tsinghua Open Source Mirror](https://mirrors.tuna.tsinghua.edu.cn/help/anaconda/)下载相关内容。

1. 首先点进[Miniconda 安装包的下载链接列表](https://mirrors.bfsu.edu.cn/anaconda/miniconda/?C=M&O=D)，按照Date排序后，根据自己的电脑操作系统，选择最新的一个下载，比如本文截稿时，最新windows 64位，搭配Python3.9的Minconda的安装文件为：[Miniconda3-py39_22.11.1-1-Windows-x86_64.exe](https://mirrors.bfsu.edu.cn/anaconda/miniconda/Miniconda3-py39_22.11.1-1-Windows-x86_64.exe "Miniconda3-py39_22.11.1-1-Windows-x86_64.exe")，一个53M的文件，不用犹豫，下载安装就是了。  
**这里要提醒的是Python3.9开始不支持Win7操作系统了，电脑使用Win7的小伙伴注意下载[Python3.8](https://mirrors.bfsu.edu.cn/anaconda/miniconda/Miniconda3-py38_22.11.1-1-Windows-x86_64.exe)。**
   ![](0-start-to-learn-python3_20230115231100.png)
2. 下载后双击安装，我建议安装在`D:\dev\miniconda3`路径下，至于为什么，问就是不喜欢装在C盘下，也不喜欢编程类文件安装路径有空格，建议大家也养成这个习惯，可以少掉几个坑。
   ![](0-start-to-learn-python3_20230115231219.png)
   ![](0-start-to-learn-python3_20230115231631.png)
   ![](0-start-to-learn-python3_20230115232005.png)
3. 经过不太漫长的等待，安装finish后，可以看看开始菜单里，应该多了几个可以新Anaconda程序，笔者个人习惯使用`Anaconda Prompt`。
   ![](0-start-to-learn-python3_20230115232220.png)
4. 运行后，出现个黑黑的命令提示符，并且前面有个`(base)`字样，说明大功告成了。此时你输入`python`，你就会看到成功运行了Python程序。
   ![](0-start-to-learn-python3_20230115232226.png)
5. 我们来输入一个编程界的著名功能，显示`hello world`：
   ```python3
   >>> print('hello world!')
	hello world!		
   ```
   当然，也可以拿来当作大型计算器先用起来：
   ```python3
   >>> 2 + 3 * 6
   20

   >>> 5+7+1
   13

   >>> 500*123
   61500

   >>> 2**8
   256

   >>> 32/4
   8.0

   >>> (5+10)*12
   180
   ```
6. 最后可以敲`exit()`或者`ctrl+Z`回车退出。

**恭喜你，你现在可以在自己简历里写上：熟练使用Python了！**