---
title: 【Python3学习-番外篇】AI辅助、指导编程，真香！
tags:
  - Python3学习
  - pandas
categories:
  - 编程
date: 2023-03-02 16:20:57
description:
---

## 太长不看版本

用AI学/来编程真香呀！
- chatGPT：如雷贯耳，需要科学上
- [Perplexity AI](https://www.perplexity.ai)：需要科学上
- [Phind: AI search engine](https://www.phind.com/)：需要科学上
- HuggingChat、bito
- claude：替代chatgpt
- CodeWhisperer：亚马逊的
- CodeGeex：清华大学的
- 等等等等

## 怎么快速获取简单Python脚本？

小波是小马的同事，近期也在和小马一起学Python。一天小波要对2份数据进行vlookup，但是一个Execl没法解决的数据量，于是就求助小马编程解决。

小马手头正好比较忙，于是打开浏览器用中文描述了一番，结果立马出来了，小马自己都被震惊到了。
<!-- more -->
![](write-with-ai.png)

AI目前这真的是距离贾维斯不远了，用来学习编程、解释代码也是一个超级助手，值得深入了解。