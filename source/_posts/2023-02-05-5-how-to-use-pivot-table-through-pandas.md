---
title: 【Python3学习】5.如何使用pandas的透视图（pivot_table）？
tags:
  - Python3学习
  - pandas
categories:
  - 编程
date: 2023-02-05 23:45:40
description:
---

## 太长不看版本

Python/pandas中怎么做透视图？
答：会用pivot_table函数即可。
<!-- more -->
### 案例数据准备
用文本编辑器输入以下内容，另存为`D:\workspace\in.csv`。
```
name,money,class,region
mike,1230,A CLASS,city
mark,3210,A CLASS,country
kite,2130,A CLASS,city
fly,3120,A CLASS,country
sky,2013,B CLASS,city
cat,3021,B CLASS,country
dog,2413,B CLASS,city
pig,3721,B CLASS,country
```
### 具体操作
- [利用pd.pivot_table函数来匹配数据](#pivot)

## 怎么弄个透视图呀？

书接上文。
小马学会了匹配数据，半小时就完成了数据匹配。他得意的看了看已经发送出去的邮件，像是想起什么似的问了问：“除了vlookup，我们还经常用excel做透视表和图标，比用公式快不少，用python，或者说用pandas怎么实现呢？”


#### <a id='pivot'>根据区域统计钱的均值和总值</a>
“那我们就先说最常见的透视表怎么做吧，”胖驼说。“还是我们先准备一点数据来统计，你先现在这个in.csv这个表，比如我们先统计一下这个班组不同区域（region）的平均工资和总工资是多少。“

```python
import pandas as pd # 导入pandas包
df = pd.read_csv(r'D:\workspace\in.csv') # 读取文件存放至df变量，所有字段取默认值，比如money字段全是数字，则pandas会自动识别为数字。
result_df = pd.pivot_table(df,values='money',index='region',aggfunc=['mean','sum']) #pd.pivot_table就是透视图函数了，values就是需要统计的内容，比如我们求money列的平均数和求和；index就是左边的列，aggfunc就是需要统计的方式，'mean'是平均数，'sum'是求和。 
result_df
```

从样本的输出结果我们可知，城市区域平均工资1946.5比乡村3268还低。
```cmd
           mean    sum
          money  money
region
city     1946.5   7786
country  3268.0  13072
```

### 根据班级和区域统计钱的均值和总值
#### 方法1
“如果要更多的分类看平均金额怎么办呢？”
```python
result_df = pd.pivot_table(df,values='money',index=['region','class'],aggfunc=['mean']) #pd.pivot_table就是透视图函数了，values就是需要统计的内容，比如我们求money列的平均数和求和；index就是左边的列，aggfunc就是需要统计的方式，'mean'是平均数，'sum'是求和。 
result_df
```
结果：
```cmd
                 mean
                money
region  class
city    A CLASS  1680
        B CLASS  2213
country A CLASS  3165
        B CLASS  3371
```
#### 方法2
“使用EXCEL有时候我们还会使用到‘列’，我们还是用刚才那个例子，只看平均值的话，不同班级、不同区域结果是什么样。”
```python
result_df = pd.pivot_table(df,values='money',index=['region'],columns=['class'],aggfunc=['mean']) #pd.pivot_table就是透视图函数了，values就是需要统计的内容，比如我们求money列的平均数和求和；index就是左边的列，aggfunc就是需要统计的方式，'mean'是平均数，'sum'是求和。 
result_df
```
结果：
```cmd
           mean
class   A CLASS B CLASS
region
city       1680    2213
country    3165    3371
```
“pivot_table方法十分强大，日常简单应用基本可以替代groupby方法，进一步学习可以借助一下搜索引擎中搜出的优秀博文了，如：[Python中pandas透视表pivot_table功能详解（非常简单易懂） - The-Chosen-One - 博客园 (cnblogs.com)](https://www.cnblogs.com/Yanjy-OnlyOne/p/11195621.html)